import random

from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

shapes = ['TRIANGLE', 'SQUARE', 'CIRCLE', 'RECTANGLE']
size = range(768)


class GeometryGenerator(Resource):
    def get(self):
        last = {}
        first = {}
        pos = sorted([random.choice(size), random.choice(size)])
        first['x'] = pos[0]
        last['x'] = pos[1]
        pos = sorted([random.choice(size), random.choice(size)])
        first['y'] = pos[0]
        last['y'] = pos[1]
        return {
            'shape': random.choice(shapes),
            'width': last['x'] - first['x'],
            'height': last['y'] - first['y'],
            'pos': first
        }


api.add_resource(GeometryGenerator, '/')

if __name__ == '__main__':
    app.run()
